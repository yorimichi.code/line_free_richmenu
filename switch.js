const line = require("@line/bot-sdk");
const fs = require("fs");

const client = new line.Client({
    channelSecret: '8b15c636b8bea0405a6ee9632a8ea88b',
    channelAccessToken: 'GC6oSzptN6qW8VEtnBMRcATXNB5cXvUFc8vf5Gn+V99I22gHswY48WZ1SITolVLCfqXwpdTe/XHQRcbS/PBB54P5pdueXivaql+NbYIZ+7Sz2xjAttWAWkBagv+U/gcFbDxZ7Ar0gwWzyt1/8U23wwdB04t89/1O/w1cDnyilFU='
});

// 1.デフォルトのリッチメニューを作成
CreateRichmenuA();
// 2. 切り替え先のリッチメニューを作成
// CreateRichmenuB();
// 3. 2つのリッチメニューをつなげるエイリアスを作成
// client.createRichMenuAlias('richmenu-460262252c508f19efffd331725d13f5', 'richmenu-alias-a');
// client.createRichMenuAlias('richmenu-afce748757653f9049287297b8ce4ab0', 'richmenu-alias-b');

function CreateRichmenuA() {
    const WIDTH = 2500;
    const HEIGHT = 1686;
    const TAB_HEIGHT = 350;

    client.createRichMenu({
        "size": {
            "width": WIDTH,
            "height": HEIGHT
        },
        "selected": true,
        "name": "よりみちプログラミング",
        "chatBarText": "メニューA",
        "areas": [
            {
                "bounds": {
                    "x": WIDTH / 2,
                    "y": 0,
                    "width": WIDTH / 2,
                    "height": TAB_HEIGHT
                },
                "action": {
                    "type": "richmenuswitch",
                    "richMenuAliasId": "richmenu-alias-b",
                    "data": "change to B"
                }
            },
        ]
    })
    .then(async (richMenuId) => {
        console.log('richMenuId A: ', richMenuId);
        const buffer = await fs.createReadStream('./assets/switch_01.png');
        await client.setRichMenuImage(richMenuId, buffer);
        await client.setDefaultRichMenu(richMenuId);
    })
}

function CreateRichmenuB() {
    const WIDTH = 2500;
    const HEIGHT = 1686;
    const TAB_HEIGHT = 350;

    client.createRichMenu({
        "size": {
            "width": WIDTH,
            "height": HEIGHT
        },
        "selected": true,
        "name": "よりみちプログラミング",
        "chatBarText": "メニューB",
        "areas": [
            {
                "bounds": {
                    "x": 0,
                    "y": 0,
                    "width": WIDTH / 2,
                    "height": TAB_HEIGHT
                },
                "action": {
                    "type": "richmenuswitch",
                    "richMenuAliasId": "richmenu-alias-a",
                    "data": "change to A"
                }
            },
        ]
    })
    .then(async (richMenuId) => {
        console.log('richMenuId B: ', richMenuId);
        const buffer = await fs.createReadStream('./assets/switch_02.png');
        await client.setRichMenuImage(richMenuId, buffer);
    })
}











deleteRichmenu();
deleteRichmenuAlias();

function deleteRichmenu() {
    client.getRichMenuList()
    .then((richmenus) => {
        richmenus.forEach(async (richmenu) => {
            await client.deleteRichMenu(richmenu.richMenuId)
        });
    })
}

function deleteRichmenuAlias() {
    client.getRichMenuAliasList()
    .then(res => {
        res.aliases.map(async item => {
            await client.deleteRichMenuAlias(item.richMenuAliasId);
        })
    })
}